#!/usr/local/bin/fish

set -g running 0

function run_dashboard
  cd ~/dev/ssh-monit
  . bin/activate.fish
  set -x FLASK_APP index.py
  set -x FLASK_ENV development
  flask run
end

# Check
ps aux | grep flask | grep ssh-monit > /dev/null ^ /dev/null
and set running 1
or set running 0

if test $running -eq 0
  echo "DSH ● | color=red"
  echo "---"
  echo "NOT RUNNING"
  echo "Will be run at next refresh"
  run_dashboard
else
  echo "DSH ● | color=green"
  echo "---"
  echo "RUNNING"
  echo "Open the dashboard | href=http://127.0.0.1:5000"
end